package test.ex.u.parsehtml;

import java.util.ArrayList;

public class Validation {

    public static boolean validationData(ArrayList<String> parsedUrls, String checkUrl) {

        if (!parsedUrls.contains(checkUrl)) {
            parsedUrls.add(checkUrl);
            return true;
        }
        return false;
    }
}
