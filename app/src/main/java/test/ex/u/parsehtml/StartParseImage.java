package test.ex.u.parsehtml;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;

public class StartParseImage {


    // parse document HTML for found URL contains image
    public StartParseImage(String url) {

        Document downloadingHtmlPage;

        //The connect(String url) method creates a new Connection, and get() fetches and parses a HTML file
        try {
            downloadingHtmlPage = Jsoup.connect(url).timeout(5000).get();
            Elements selectAttributeSRC = downloadingHtmlPage.select("[src]");

            for (Element attributeSRC : selectAttributeSRC) {

                if (attributeSRC.tagName().equals("img")) {
                    String image = attributeSRC.attr("abs:src");

                    if (image == null || image.length() == 0)
                        continue;
                    //check if URL contains image with a type of png, jpg, gif
                    if (image.endsWith("png") || image.endsWith("jpg") || image.endsWith("gif")) {

                        // check if the link has already been parsed
                        if (Validation.validationData(Singleton.getInstance().getUniqueImageNamesStorage(), image))
                            new DownloadingImage(image);
                    }
                }
            }
        } catch (NullPointerException e){System.out.println("Error parse HTML file " + e.getMessage());}
        catch (IOException e) {
            System.out.println("Error loading page" + e.getMessage());
        }
    }
}
