package test.ex.u.parsehtml;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

class DownloadingImage implements Runnable {

    private String downloadingLink;
    private String nameImage;

    DownloadingImage(String downloadingLink) {
        this.downloadingLink = downloadingLink;
        nameImage = parseNameImage(downloadingLink);
        new Thread(this).start();
    }
    // From the link gets the name of the image
    private String parseNameImage(String image) {
        String parseOfTheUrlImage[] = image.split("/");
        return parseOfTheUrlImage[parseOfTheUrlImage.length - 1];
    }

    @Override
    public void run() {

        URL url;
        URLConnection connection;
        BufferedInputStream bufferedInputStream = null;
        FileOutputStream fileOutputStream = null;
        File file;

        try {
            url = new URL(downloadingLink);
            connection = url.openConnection();
            connection.setDoOutput(true);
            connection.setDoInput(true);

            bufferedInputStream = new BufferedInputStream(connection.getInputStream());
            file = new File("C:\\downloadingParseImage\\" + nameImage);
            fileOutputStream = new FileOutputStream(file);

            int ch;
            while ((ch = bufferedInputStream.read()) != -1)
                fileOutputStream.write(ch);

            bufferedInputStream.close();
            fileOutputStream.flush();
            fileOutputStream.close();

        } catch (MalformedURLException | NullPointerException |FileNotFoundException e) {
            System.out.println("Error loading file " + e.getMessage());}
          catch (IOException e){
              System.err.println("Connect to URL fail " + e.getMessage());
          }
    }
}

