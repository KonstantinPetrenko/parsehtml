package test.ex.u.parsehtml;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.ListIterator;

public class StartParse {
    private static final String WEB_PAGE = "http://uavto.kr.ua";
    private static final int HTTP_TIMEOUT = 5000;
    private static final String A_HREF = "a[href]";
    private static final String ABS_HREF = "abs:href";

    public static void main(String[] args) {
        Singleton.getInstance().getFoundedLinksStorage().add(WEB_PAGE);
        Singleton.getInstance().getParseLinkStorage().add(WEB_PAGE);
        startParserUrl(Singleton.getInstance().getParseLinkStorage());
    }

    // fetch and parse a HTML document from the web
    private static void startParserUrl(ArrayList<String>  parseLinkStorage) {

        if (parseLinkStorage.isEmpty())
            return;

        ListIterator<String> iterator = parseLinkStorage.listIterator();
        while (iterator.hasNext()) {

            Document downloadingHtmlPage;

            //The connect(String url) method creates a new Connection, and get() fetches and parses a HTML file
            try {
                downloadingHtmlPage = Jsoup.connect(iterator.next()).timeout(HTTP_TIMEOUT).get();

                if (downloadingHtmlPage == null)
                    continue;

                    // select method returns a list of found links with the a[href] attribute
                    Elements foundLinks = downloadingHtmlPage.select(A_HREF);
                    // search absolute URL
                    for (Element absoluteUrl : foundLinks) {
                        String parseAbsoluteUrl = absoluteUrl.attr(ABS_HREF);

                        if (parseAbsoluteUrl == null || parseAbsoluteUrl.length() == 0)
                            continue;

                        // check if the link has already been parsed
                        if (Validation.validationData(Singleton.getInstance().getFoundedLinksStorage(), parseAbsoluteUrl)) {
                            System.out.println("Parsed WEB PAGE " + parseAbsoluteUrl);

                            Singleton.getInstance().getNewUrlStorage().add(parseAbsoluteUrl);
                            // Passing a link to a class StartParseImage
                            new StartParseImage(parseAbsoluteUrl);
                        }
                    }
            } catch (IOException e) {
                System.out.println("connecting faled to URL web page");
                e.printStackTrace();
            }
        }
        // Found links are cloned in parseLinkStorage
        parseLinkStorage = (ArrayList<String>) Singleton.getInstance().getNewUrlStorage().clone();

        Singleton.getInstance().getNewUrlStorage().clear();
        // recursive method "startParserUrl(ArrayList<>)" call
        startParserUrl(parseLinkStorage);
    }
}