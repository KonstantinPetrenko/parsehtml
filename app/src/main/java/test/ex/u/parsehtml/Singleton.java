package test.ex.u.parsehtml;

import java.util.ArrayList;

public class Singleton {
    private static Singleton instance;

    private ArrayList<String> uniqueImageNamesStorage;
    private ArrayList<String> foundLinksStorage;
    private ArrayList<String> newUrlStorage;
    private ArrayList<String> parseLinkStorage;

    private Singleton() {
        uniqueImageNamesStorage = new ArrayList<>();
        foundLinksStorage = new ArrayList<>();
        newUrlStorage = new ArrayList<>();
        parseLinkStorage = new ArrayList<>();
    }

    static Singleton getInstance(){
        if(instance == null)
            instance = new Singleton();

        return instance;
    }
    public ArrayList<String> getUniqueImageNamesStorage() {
        return uniqueImageNamesStorage;
    }

    public ArrayList<String> getFoundedLinksStorage() {
        return foundLinksStorage;
    }

    public ArrayList<String> getNewUrlStorage() {
        return newUrlStorage;
    }

    public ArrayList<String> getParseLinkStorage() { return parseLinkStorage; }
}

